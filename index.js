const express = require("express");
const app = express();
const port = 8000;

app.use((req, res, next) => {
    let date = new Date();
    console.log(date);
    next();
});

app.use((req, res, next) => {
    console.log(req.method);
    next();
});

app.get("/", (req, res) => {
    let date = new Date();
    res.status(200).json({
        message: `Current: ${date}`
    });
});

app.listen(port, () => {
    console.log("App listening on port: ", port);
});